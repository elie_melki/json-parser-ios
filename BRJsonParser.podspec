Pod::Spec.new do |s|
  s.name         = "BRJsonParser"
  s.version      = "1.0"
  s.summary      = "iOS library"
  s.homepage     = "http://me.com"
  s.author       = { "Elie Melki" => "elie.j.melki@gmail.com" }
  s.platform     = :ios
  s.source       = { :git => "https://bitbucket.org/elie_melki/json-parser-ios.git/" , :tag => s.version }
  s.source_files =  'BRJsonParser/BRJsonParser/**/*.{h,m}'
  s.ios.deployment_target = '5.0'
  s.frameworks = 'Foundation'
  s.requires_arc = true
  s.license      = {
    :type => 'Copyright',
    :text => <<-LICENSE
      Copyright 2014-2015 Elie Melki. All rights reserved.
      LICENSE
  }
end