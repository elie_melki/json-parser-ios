//
//  BRJsonParser.h
//  BRJsonParser
//
//  Created by ELie Melki on 10/19/14.
//  Copyright (c) 2014 My Company. All rights reserved.
//

//  Version: ${PROJECT_VERSION_STRING} (Build ${PROJECT_VERSION})
//
//  Built by: ${BUILD_USER}
//        at: ${BUILD_DATE}

#import <Foundation/Foundation.h>

//! Project version number for BRJsonParser.
FOUNDATION_EXPORT double BRJsonParserVersionNumber;

//! Project version string for BRJsonParser.
FOUNDATION_EXPORT const unsigned char BRJsonParserVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BRJsonParser/PublicHeader.h>

#import "BRAbsoluteURLSerialization.h"
#import "BRDateSerialization.h"
#import "BRJSONSerialization.h"
#import "BRLocaleSerialization.h"
#import "BRObjectSerialization.h"
#import "BRRelativeURLSerialization.h"
#import "BRSerialization.h"
#import "BRTypedArraySerialization.h"
#import "NSObject+BRJsonParserProperties.h"
#import "BRUUIDSerialization.h"
#import "BRTypedDictionarySerialization.h"
#import "BRSetSerialization.h"
#import "BRTypedSetSerialization.h"
#import "BRTypedOrderedSetSerialization.h"
#import "BROrderedSetSerialization.h"
#import "BRJavaTimeIntervalSerialization.h"