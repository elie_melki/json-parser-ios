//
//  NSObject+Properties.m
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "NSObject+BRJsonParserProperties.h"

@implementation BRJsonParserProperty

@synthesize propertyName,propertyType;

- (id) initWithPropertyName:(NSString *)thePropertyName propertyType:(NSString *)thePropertyType
{
    self = [super init];
    if (self)
    {
        propertyName = thePropertyName;
        propertyType = thePropertyType;;
    }
    return self;
}


@end

@implementation NSObject (BRJsonParserProperties)

- (NSArray *) properties
{
    NSMutableArray *_properties = [NSMutableArray array];
    unsigned int outCount, i;
    objc_property_t *nproperties = class_copyPropertyList([self class], &outCount);
    for(i = 0; i < outCount; i++)
    {
        
        objc_property_t _property = nproperties[i];
        BRJsonParserProperty *_kvProperty = [self propertyFor:_property];
        if (_kvProperty)
            [_properties addObject:_kvProperty];
    }
    free(nproperties);
    return _properties;
}

- (BRJsonParserProperty *) propertyFor:(objc_property_t)theProperty
{
    objc_property_t _property = theProperty;
    const char *_propName = property_getName(_property);
    
    if(_propName)
    {
        const char *_propType = getPropertyType(_property);
        NSString *_propertyName = [NSString stringWithCString:_propName encoding:NSUTF8StringEncoding];
        NSString *_propertyType =  [NSString stringWithCString:_propType  encoding:NSUTF8StringEncoding];
        
        return [[BRJsonParserProperty alloc] initWithPropertyName:_propertyName propertyType:_propertyType];
    }
    return nil;
}

- (BRJsonParserProperty *) propertyWithName:(NSString *)theName
{
    objc_property_t _nproperty = class_getProperty([self class], [theName UTF8String]);
    BRJsonParserProperty *_kvProperty = [self propertyFor:_nproperty];
    return _kvProperty;
}

static const char *getPropertyType(objc_property_t property)
{
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T') {
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    return "@";
}

@end
