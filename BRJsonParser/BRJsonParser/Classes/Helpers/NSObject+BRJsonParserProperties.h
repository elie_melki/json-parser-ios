//
//  NSObject+Properties.h
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>


@interface BRJsonParserProperty : NSObject

@property (nonatomic,readonly) NSString *propertyType;
@property (nonatomic,readonly) NSString *propertyName;

@end

@interface NSObject (BRJsonParserProperties)

- (NSArray *) properties;
- (BRJsonParserProperty *) propertyWithName:(NSString *)theName;
- (BRJsonParserProperty *) propertyFor:(objc_property_t)theProperty;

@end
