//
//  BRJavaTimeSerialization.m
//  LXProxSeeSDK
//
//  Created by ELie Melki on 7/18/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import "BRJavaTimeIntervalSerialization.h"

@implementation BRJavaTimeIntervalSerialization

+ (BRJavaTimeIntervalSerialization *) javaTimeIntervalSerialization
{
    return [[self alloc] init];
}

//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSDate class]])
    {
        NSDate *date = (NSDate *)theObject;
        NSTimeInterval timeInterval = [date timeIntervalSince1970] * 1000;
        long long llTimeInterval = (long long)timeInterval;
        NSString *v = [NSString stringWithFormat:@"%@",  [NSNumber numberWithLongLong:llTimeInterval]];
        return v;
    }
    else
        return nil;
    
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
    {
        NSString *data = (NSString *)theData;
        long long llDate =  [data longLongValue];
        NSTimeInterval timeInterval = llDate/ 1000;
        
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        return date;
    }
    else if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"%s[Line %d]: Expected an NSString for %@ found other %@",__PRETTY_FUNCTION__, __LINE__, theData, [theData class]]
                                     userInfo:nil];
    return nil;
}


@end