//
//  BRJavaTimeSerialization.h
//  LXProxSeeSDK
//
//  Created by ELie Melki on 7/18/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRJsonParser.h"

@interface BRJavaTimeIntervalSerialization : NSObject<BRSerialization>

+ (BRJavaTimeIntervalSerialization *) javaTimeIntervalSerialization;

@end