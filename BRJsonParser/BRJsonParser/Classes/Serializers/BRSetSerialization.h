//
//  BRNSSetSerialization.h
//  LXProxSeeSDK
//
//  Created by ELie Melki on 4/25/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRSetSerialization : NSObject<BRSerialization>

+ (BRSetSerialization *) setSerialization;

@end
