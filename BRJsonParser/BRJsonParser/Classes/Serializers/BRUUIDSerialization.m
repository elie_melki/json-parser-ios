//
//  BRUUIDSerialization.m
//  BRJsonParser
//
//  Created by ELie Melki on 1/31/15.
//  Copyright (c) 2015 My. All rights reserved.
//

#import "BRUUIDSerialization.h"

@implementation BRUUIDSerialization

+ (BRUUIDSerialization *) UUIDSerialization
{
    return [[self alloc] init];
}


- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSUUID class]])
    {
        NSUUID *uuid = (NSUUID *)theObject;
        return uuid.UUIDString;
    }
    else
        return nil;
    
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
        return [[NSUUID alloc] initWithUUIDString:theData];
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSString for %@ found other %@", theData, [theData class]]
                                     userInfo:nil];
    return nil;
}

@end