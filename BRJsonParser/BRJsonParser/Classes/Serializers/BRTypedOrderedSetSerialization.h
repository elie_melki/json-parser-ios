//
//  BRTypedOrderedSetSerialization.h
//  LXProxSeeSDK
//
//  Created by ELie Melki on 7/18/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRObjectSerialization.h"

@interface BRTypedOrderedSetSerialization : NSObject<BRSerialization>

+ (BRTypedOrderedSetSerialization *) typedOrderedSetSerializationWith:(id<BRSerialization>)theSerializer;

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer;

@end



