//
//  KVLocaleSerialization.m
//  Rewardisement
//
//  Created by ELie Melki on 11/23/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRLocaleSerialization.h"

@implementation BRLocaleSerialization

+ (BRLocaleSerialization *) localeSerialization
{
    return [[self alloc] init] ;
}

//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSLocale class]])
    {
        NSLocale *_locale = (NSLocale *)theObject;
        return _locale.localeIdentifier;
    }
    else
        return nil;

    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
        return [[NSLocale alloc] initWithLocaleIdentifier:theData] ;
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSString for %@ found other %@", theData, [theData class]]
                                     userInfo:nil];
    return nil;
}


@end
