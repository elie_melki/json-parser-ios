//
//  KVTypedArraySerializer.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRObjectSerialization.h"

@interface BRTypedArraySerialization : NSObject<BRSerialization>

+ (BRTypedArraySerialization *) typedArraySerializationWith:(id<BRSerialization>)theSerializer;

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer;

@end
