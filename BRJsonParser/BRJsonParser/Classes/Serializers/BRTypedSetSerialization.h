//
//  BRTypedSetSerialization.h
//  LXProxSeeSDK
//
//  Created by ELie Melki on 5/4/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRObjectSerialization.h"

@interface BRTypedSetSerialization : NSObject<BRSerialization>

+ (BRTypedSetSerialization *) typedSetSerializationWith:(id<BRSerialization>)theSerializer;

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer;

@end
