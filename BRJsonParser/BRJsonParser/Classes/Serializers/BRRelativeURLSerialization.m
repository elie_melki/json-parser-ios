//
//  KVRelativeURLSerialization.m
//  Rewardisement
//
//  Created by ELie Melki on 11/28/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRRelativeURLSerialization.h"

@interface BRRelativeURLSerialization()

@property (nonatomic,copy) NSURL *baseURL;

@end

@implementation BRRelativeURLSerialization

+ (BRRelativeURLSerialization *) relativeSerializationWithBaseURL:(NSURL *)theBaseURL
{
    return [[self alloc] initWithBaseURL:theBaseURL] ;
}

@synthesize baseURL;

- (id) initWithBaseURL:(NSURL *)theBaseURL
{
    self = [super init];
    
    if (self)
    {
        self.baseURL = theBaseURL;
    }
    return self;
}




- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSURL class]])
    {
        NSURL *_URL = (NSURL *)theObject;
        return [_URL relativeString];
    }
    else
        //Todo  probably throw an exception;
        return nil;
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
        return [NSURL URLWithString:theData relativeToURL:self.baseURL];
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSString for %@ found other", theData]
                                     userInfo:nil];
    return nil;
}

@end
