//
//  BRTypedDictionarySerialization.h
//  BRJsonParser
//
//  Created by ELie Melki on 2/1/15.
//  Copyright (c) 2015 My. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"


//Execpt a key for type NSString

@interface BRTypedDictionarySerialization : NSObject<BRSerialization>

+ (BRTypedDictionarySerialization *) typedDictionarySerializationWith:(id<BRSerialization>)theSerializer;

- (id) initWithSerializer:(id<BRSerialization>)theSerializer;

@end
