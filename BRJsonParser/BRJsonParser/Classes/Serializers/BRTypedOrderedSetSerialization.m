//
//  BRTypedOrderedSetSerialization.m
//  LXProxSeeSDK
//
//  Created by ELie Melki on 7/18/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import "BRTypedOrderedSetSerialization.h"
#import "BRTypedArraySerialization.h"


@interface BRTypedOrderedSetSerialization()

@property (nonatomic, strong) BRTypedArraySerialization *typedArraySerialization;

@end

@implementation BRTypedOrderedSetSerialization

+ (BRTypedOrderedSetSerialization *) typedOrderedSetSerializationWith:(id<BRSerialization>)theSerializer
{
    return [[self alloc] initWithValueSerializer:theSerializer] ;
}


//------------------------------------
//Init & Dealloc
//------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer
{
    self = [super init];
    if (self)
    {
        self.typedArraySerialization = [BRTypedArraySerialization typedArraySerializationWith:theValueSerializer];
    }
    return self;
}



//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSOrderedSet class]])
    {
        NSOrderedSet *_data = (NSOrderedSet *)theObject;
        return [self.typedArraySerialization serialize:[_data array]];
    }
    else
    {
        //Todo throw an exception;
        return nil;
    }
}

- (id) deserialize:(id)theData
{
    NSArray *data = [self.typedArraySerialization deserialize:theData];
    if (data != nil)
    {
        return [NSOrderedSet orderedSetWithArray:data];
    }
    else
    {
        return nil;
    }
}


@end