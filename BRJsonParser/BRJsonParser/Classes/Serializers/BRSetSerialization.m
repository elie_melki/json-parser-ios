//
//  BRNSSetSerialization.m
//  LXProxSeeSDK
//
//  Created by ELie Melki on 4/25/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import "BRSetSerialization.h"

@implementation BRSetSerialization

+ (BRSetSerialization *) setSerialization
{
    return [[self alloc] init];
}

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSSet class]])
    {
        NSSet *set = (NSSet *)theObject;
        return [set allObjects];
    }
    else
        return nil;
    
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSArray class]])
        return [[NSSet alloc] initWithArray:theData];
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSArray for %@ found other %@", theData, [theData class]]
                                     userInfo:nil];
    return nil;
}


@end
