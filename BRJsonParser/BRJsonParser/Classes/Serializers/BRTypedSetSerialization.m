//
//  BRTypedSetSerialization.m
//  LXProxSeeSDK
//
//  Created by ELie Melki on 5/4/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import "BRTypedSetSerialization.h"
#import "BRTypedArraySerialization.h"

@interface BRTypedSetSerialization()

@property (nonatomic, strong) BRTypedArraySerialization *typedArraySerialization;

@end

@implementation BRTypedSetSerialization

+ (BRTypedSetSerialization *) typedSetSerializationWith:(id<BRSerialization>)theSerializer
{
    return [[self alloc] initWithValueSerializer:theSerializer] ;
}


//------------------------------------
//Init & Dealloc
//------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer
{
    self = [super init];
    if (self)
    {
        self.typedArraySerialization = [BRTypedArraySerialization typedArraySerializationWith:theValueSerializer];
    }
    return self;
}



//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSSet class]])
    {
        NSSet *_data = (NSSet *)theObject;
        return [self.typedArraySerialization serialize:[_data allObjects]];
    }
    else
    {
        //Todo throw an exception;
        return nil;
    }
}

- (id) deserialize:(id)theData
{
    NSArray *data = [self.typedArraySerialization deserialize:theData];
    if (data != nil) {
        return [NSSet setWithArray:data];
    }else
    {
        return nil;
    }
}

@end
