//
//  KVURLSerializer.m
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRAbsoluteURLSerialization.h"

@implementation BRAbsoluteURLSerialization

+ (BRAbsoluteURLSerialization *) absoluteURLSerialization
{
    return [[self alloc] init] ;
}

//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSURL class]])
    {
        NSURL *_URL = (NSURL *)theObject;
        return [_URL absoluteString];
    }
    else
        return nil;

}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
        return [NSURL URLWithString:theData];
    else if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSString for %@ found other", theData]
                                     userInfo:nil];
    return nil;
}


@end
