//
//  BROrderedSetSerialization.m
//  LXProxSeeSDK
//
//  Created by ELie Melki on 7/18/15.
//  Copyright (c) 2015 Lixar. All rights reserved.
//

#import "BROrderedSetSerialization.h"

@implementation BROrderedSetSerialization

+ (BROrderedSetSerialization *) orderedSetSerialization
{
    return [[self alloc] init];
}

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSOrderedSet class]])
    {
        NSOrderedSet *set = (NSOrderedSet *)theObject;
        return [set array];
    }
    else
        return nil;
    
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSArray class]])
        return [NSOrderedSet orderedSetWithArray:theData];
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an NSArray for %@ found other %@", theData, [theData class]]
                                     userInfo:nil];
    return nil;
}


@end
