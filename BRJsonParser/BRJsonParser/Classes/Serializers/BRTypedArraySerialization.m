//
//  KVTypedArraySerializer.m
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRTypedArraySerialization.h"

@interface BRTypedArraySerialization()

@property (nonatomic, retain) id<BRSerialization> valueSerializer;

@end

@implementation BRTypedArraySerialization

@synthesize valueSerializer;

+ (BRTypedArraySerialization *) typedArraySerializationWith:(id<BRSerialization>)theSerializer
{
    return [[self alloc] initWithValueSerializer:theSerializer] ;
}


//------------------------------------
//Init & Dealloc
//------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithValueSerializer:(id<BRSerialization>)theValueSerializer
{
    self = [super init];
    if (self)
    {
        self.valueSerializer = theValueSerializer;
    }
    return self;
}



//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *_r = [NSMutableArray array];
        NSArray *_data = (NSArray *)theObject;
        for (id _elem in _data)
        {
            id _serializedElement = [valueSerializer serialize:_elem];
            [_r addObject:_serializedElement];
        }
        return _r;
    }
    else
    {
        //Todo throw an exception;
        return nil;
    }
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSArray class]])
    {
        NSMutableArray *_r = [NSMutableArray array];
        NSArray *_data = (NSArray *)theData;
        for (id _elem in _data)
        {
            id _deserializedElement = [valueSerializer deserialize:_elem];
            [_r addObject:_deserializedElement];
        }
        return _r;
    }
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
    {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"Expected an array  for %@ found other [%@]", theData, [theData class]]
                                     userInfo:nil];
    }
    return nil;
}


@end
