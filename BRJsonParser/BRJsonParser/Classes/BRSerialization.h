//
//  KVSerializable.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BRSerialization <NSObject>

- (id) deserialize:(id)theData;

- (id) serialize:(id)theObject;

@end
