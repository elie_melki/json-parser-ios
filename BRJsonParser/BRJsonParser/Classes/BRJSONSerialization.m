//
//  KVJSONSerialization.m
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRJSONSerialization.h"
#import "BRLogger.h"

@implementation BRJSONSerialization

+ (id) deserialize:(id)object error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable
{
    @try
    {
        if (object == nil)
            return nil;
        else
            return [theSerializable deserialize:object];
    }
    @catch (NSException *exception)
    {
        if (theError != NULL)
        {
            *theError = [NSError errorWithDomain:exception.reason code:12  userInfo:nil] ;
            BRLogError(@"Deserialize Failed with Error: %@",exception.reason);
            return nil;
        }
    }
    @finally
    {
        
    }
}

+ (id) deserializeWithData:(NSData *)theData options:(NSJSONReadingOptions)theOption error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable
{
    @try
    {
        NSError *_error = NULL;
        id _object = [NSJSONSerialization JSONObjectWithData:theData options:theOption error:&_error];
        
        if (_error)
           @throw [NSException exceptionWithName:@"Json Derserializer Exception" reason:[NSString stringWithFormat:@"%@",_error] userInfo:nil];
        
        if (_object == nil)
            return nil;
        else
            return [theSerializable deserialize:_object];

        
    }
    @catch (NSException *exception)
    {
        if (theError != NULL)
        {
            *theError = [NSError errorWithDomain:exception.reason code:12  userInfo:nil] ;
            BRLogError(@"Deserialize Failed with Error: %@",exception.reason);
            return nil;
        }
    }
    @finally
    {
        
    }
    return nil;
}

+ (NSData *) serializeWithData:(id)theObject options:(NSJSONWritingOptions)theOption error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable
{
    @try
    {
        if (theObject == nil)
            return nil;
        else
        {
            id _object = [theSerializable serialize:theObject];
            
            NSError *_error = NULL;
            NSData *_result = [NSJSONSerialization dataWithJSONObject:_object options:theOption error:&_error];
            
            if (_error)
                @throw [NSException exceptionWithName:@"Json Serializer Exception" reason:[NSString stringWithFormat:@"%@",_error] userInfo:nil];
            
            return _result;
        }
    }
    @catch (NSException *exception)
    {
        if (theError != NULL)
        {
            *theError = [NSError errorWithDomain:exception.reason code:0  userInfo:nil];
            BRLogError(@"Serializer Failed  with Error: %@",exception.reason);
            return nil;
        }
    }
    @finally
    {
        
    }
    return nil;
}

+ (id) serialize:(id)theObject error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable
{
    @try
    {
        if (theObject == nil)
            return nil;
        else
        {
            id _object = [theSerializable serialize:theObject];
            
            return _object;
        }
    }
    @catch (NSException *exception)
    {
        if (theError != NULL)
        {
            *theError = [NSError errorWithDomain:exception.reason code:0  userInfo:nil];
            BRLogError(@"Serializer Failed  with Error: %@",*theError);
            return nil;
        }
    }
    @finally
    {
        
    }
    return nil;
}

#pragma mark - Async

+ (void) deserializeWithData:(NSData *)theData options:(NSJSONReadingOptions)theOption serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *error = nil;
        id object = [BRJSONSerialization deserializeWithData:theData options:theOption error:&error serialization:theSerializable];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (error) {
                onError(error);
            }else {
                onSuccess(object);
            }
        });
    });
}

+ (void) deserialize:(id)object serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *error = nil;
        id result = [BRJSONSerialization deserialize:object error:&error serialization:theSerializable];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (error) {
                onError(error);
            }else {
                onSuccess(result);
            }
        });
    });
}

+ (void) serializeWithData:(id)theObject options:(NSJSONWritingOptions)theOption serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(NSData *))onSuccess onError:(void (^)(NSError *))onError
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *error = nil;
        id object = [BRJSONSerialization serializeWithData:theObject options:theOption error:&error serialization:theSerializable];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (error) {
                onError(error);
            }else {
                onSuccess(object);
            }
        });
    });
}
+ (void) serialize:(id)theObject serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *error = nil;
        id object = [BRJSONSerialization serialize:theObject error:&error serialization:theSerializable];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (error) {
                onError(error);
            }else {
                onSuccess(object);
            }
        });
    });
}

@end
